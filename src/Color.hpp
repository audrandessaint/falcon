#pragma once

#include <compare>
#include <algorithm>
#include <cmath>
#include <xmmintrin.h>

#ifdef _DEBUG
#include <iostream>
#endif

#include <iostream>

namespace falcon
{
    class Color
    {
        enum Space
        {
            RGB,
            HSV
        };

    public:
        Color() : Color(0, 0, 0, 0)
        {
        }
        Color(float r, float g, float b, float a = 1.f) : m_space(RGB)
        {
            m_channels[0] = r;
            m_channels[1] = g;
            m_channels[2] = b;
            m_channels[3] = a;
        }
        Color(float c, float a = 1.f) : Color(c, c, c, a) {}
        Color(__m128 channels) : m_simd_channels(channels) {}
        Color(__m128 channels, float alpha) : m_simd_channels(channels) { m_channels[3] = alpha; }
        ~Color() {}

        inline float red() const { return m_channels[0]; }
        inline float green() const { return m_channels[1]; }
        inline float blue() const { return m_channels[2]; }
        inline float alpha() const { return m_channels[3]; }
        inline float &red() { return m_channels[0]; }
        inline float &green() { return m_channels[1]; }
        inline float &blue() { return m_channels[2]; }
        inline float &alpha() { return m_channels[3]; }

        inline float hue() const { return m_channels[0]; }
        inline float saturation() const { return m_channels[1]; }
        inline float value() const { return m_channels[2]; }
        inline float &hue() { return m_channels[0]; }
        inline float &saturation() { return m_channels[1]; }
        inline float &value() { return m_channels[2]; }

        void toRGB()
        {
            switch (m_space)
            {
            case RGB:
                break;
            case HSV:
                HSV2RGB();
                break;
            }
        }

        void toHSV()
        {
            switch (m_space)
            {
            case RGB:
                RGB2HSV();
                break;
            case HSV:
                break;
            }
        }

        inline float gray() const { return red() * 0.299f + green() * 0.587f + blue() * 0.114f; }
        inline float average() const { return (red() + green() + blue()) / 3.f; }

        inline void clamp();

        inline void gammaCorrection(float gamma = 2.2f)
        {
            m_channels[0] = std::pow(m_channels[0], 1.f / gamma);
            m_channels[1] = std::pow(m_channels[1], 1.f / gamma);
            m_channels[2] = std::pow(m_channels[2], 1.f / gamma);
        }

        static const Color White;
        static const Color Black;
        static const Color Gray;
        static const Color Red;
        static const Color Green;
        static const Color Blue;
        static const Color Yellow;
        static const Color Transparent;

        inline std::partial_ordering operator<=>(const Color &other) const { return average() <=> other.average(); }

        friend inline void operator+=(Color &, const Color &);
        friend inline void operator-=(Color &, const Color &);
        friend inline void operator*=(Color &, const Color &);

        friend inline void operator+=(Color &, float);
        friend inline void operator-=(Color &, float);
        friend inline void operator*=(Color &, float);
        friend inline void operator/=(Color &, float);

#ifdef _DEBUG
        friend inline std::ostream &operator<<(std::ostream &, const Color &);
#endif

    private:
        static inline void clamp(float &value) { value = std::clamp(value, 0.f, 1.f); }
        inline void HSV2RGB()
        {
            float H = hue() / 60.f;
            float F = H - static_cast<int>(H);

            float a = value() * (1.f - saturation());
            float b = value() * (1.f - saturation() * F);
            float c = value() * (1.f - (saturation() * (1.f - F)));

            float red, green, blue;
            switch (static_cast<int>(H))
            {
            case 0:
                red = value();
                green = c;
                blue = a;
                break;
            case 1:
                red = b;
                green = value();
                blue = a;
                break;
            case 2:
                red = a;
                green = value();
                blue = c;
                break;
            case 3:
                red = a;
                green = b;
                blue = value();
                break;
            case 4:
                red = c;
                green = a;
                blue = value();
                break;
            case 5:
                red = value();
                green = a;
                blue = b;
                break;
            default:
                red = 255;
                green = 255;
                blue = 255;
                break;
            }

            m_channels[0] = red;
            m_channels[1] = green;
            m_channels[2] = blue;
            m_space = RGB;
        }

        inline void RGB2HSV()
        {
            float c_max = std::max(std::max(red(), green()), blue());
            float c_min = std::min(std::min(red(), green()), blue());
            float delta = c_max - c_min;

            float hue;
            if (delta == 0)
            {
                hue = 0;
            }
            else if (c_max == red())
            {
                hue = 60.f * ((green() - blue()) / delta);
            }
            else if (c_max == green())
            {
                hue = 60.f * ((blue() - red()) / delta + 2.f);
            }
            else
            {
                hue = 60.f * ((red() - green()) / delta + 4.f);
            }

            if (hue < 0)
            {
                hue += 360.f;
            }

            float saturation = 0.f;
            if (c_max != 0)
            {
                saturation = delta / c_max;
            }

            float value = c_max;

            m_channels[0] = hue;
            m_channels[1] = saturation;
            m_channels[2] = value;
            m_space = HSV;
        }

        Space m_space;

        union
        {
            __m128 m_simd_channels;
            alignas(32) float m_channels[4];
        };
    };

    void Color::clamp()
    {
        m_simd_channels = _mm_min_ps(_mm_max_ps(m_simd_channels, _mm_set1_ps(0.f)), _mm_set1_ps(1.f));
    }

    inline Color operator+(Color, const Color &);
    inline Color operator-(Color, const Color &);
    inline Color operator*(Color, const Color &);

    inline Color operator+(Color, float);
    inline Color operator-(Color, float);
    inline Color operator*(Color, float);
    inline Color operator/(Color, float);

    inline Color operator+(float, Color);
    inline Color operator-(float, Color);
    inline Color operator*(float, Color);

    Color operator+(Color c1, const Color &c2)
    {
        c1 += c2;
        return c1;
    }

    Color operator-(Color c1, const Color &c2)
    {
        c1 -= c2;
        return c1;
    }

    Color operator*(Color c1, const Color &c2)
    {
        c1 *= c2;
        return c1;
    }

    void operator+=(Color &c1, const Color &c2)
    {
        // behavior for alpha channel?
        c1.m_simd_channels = _mm_add_ps(c1.m_simd_channels, c2.m_simd_channels);
    }

    void operator-=(Color &c1, const Color &c2)
    {
        // not correct behavior for alpha channel
        c1.m_simd_channels = _mm_sub_ps(c1.m_simd_channels, c2.m_simd_channels);
    }

    void operator*=(Color &c1, const Color &c2)
    {
        // behavior for alpha channel?
        c1.m_simd_channels = _mm_mul_ps(c1.m_simd_channels, c2.m_simd_channels);
    }

    Color operator+(Color c, float f)
    {
        c += f;
        return c;
    }

    Color operator-(Color c, float f)
    {
        c -= f;
        return c;
    }

    Color operator*(Color c, float f)
    {
        c *= f;
        return f;
    }

    Color operator/(Color c, float f)
    {
        c /= f;
        return c;
    }

    Color operator+(float f, Color c)
    {
        c += f;
        return c;
    }

    Color operator-(float f, Color c)
    {
        c -= f;
        return c;
    }

    Color operator*(float f, Color c)
    {
        c *= f;
        return c;
    }

    void operator+=(Color &c, float f)
    {
        // TODO: use SIMD instructions
        c.m_channels[0] += f;
        c.m_channels[1] += f;
        c.m_channels[2] += f;
    }

    void operator-=(Color &c, float f)
    {
        // TODO: use SIMD instructions
        c.m_channels[0] -= f;
        c.m_channels[1] -= f;
        c.m_channels[2] -= f;
    }

    void operator*=(Color &c, float f)
    {
        // TODO: use SIMD instructions
        c.m_channels[0] *= f;
        c.m_channels[1] *= f;
        c.m_channels[2] *= f;
    }

    void operator/=(Color &c, float f)
    {
        // TODO: use SIMD instructions
        c.m_channels[0] /= f;
        c.m_channels[1] /= f;
        c.m_channels[2] /= f;
    }
}