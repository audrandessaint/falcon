#pragma once

#include <filesystem>
#include <atomic>
#include <vector>

namespace falcon
{
    class FileFormat
    {
        friend class Image;

    public:
        FileFormat();
        virtual ~FileFormat();

        float progress() const { return m_progress; }

    private:
        unsigned int getWidth() const { return m_width; }
        unsigned int getHeight() const { return m_height; }
        void moveToImageBuffer(class Color *);

        void setWidth(unsigned int width) { m_width = width; }
        void setHeight(unsigned int height) { m_height = height; }
        void fillBuffer(const class Color *);

    protected:
        virtual bool load(std::filesystem::path) = 0;
        virtual bool save(std::filesystem::path) const = 0;

        std::atomic<unsigned int> m_progress;
        unsigned int m_width;
        unsigned int m_height;
        std::vector<float> m_pixel_buffer;
    };

    class PPMFileFormat : public FileFormat
    {
    public:
        bool load(std::filesystem::path) override;
        bool save(std::filesystem::path) const override;
    };

    class BMPFileFormat : public FileFormat
    {
    public:
        bool load(std::filesystem::path) override;
        bool save(std::filesystem::path) const override;
    };

    class PNGFileFormat : public FileFormat
    {
    public:
        bool load(std::filesystem::path) override;
        bool save(std::filesystem::path) const override;
    };

    class EXRFileFormat : public FileFormat
    {
    public:
        bool load(std::filesystem::path) override;
        bool save(std::filesystem::path) const override;
    };
}