#include "../FileFormat.hpp"

#include <ctime>
#include <cstdio>
#ifdef _DEBUG
    #include <iostream>
#endif

#include <png.h>

using namespace falcon;

struct PNGheader
{
    uint32_t width;       /// width of the image
    uint32_t height;      /// height of the image
    uint8_t bdepth;       /// bit depth
    uint8_t colortype;    /// type of the color
    uint8_t compMode;     /// compression method
    uint8_t filterMethod; /// filter method
    uint8_t interlacing;  /// interlace method
};

static bool loadHeader(PNGheader &header, png_structp png_ptr, png_infop info_ptr)
{
    uint32_t width;
    uint32_t height;
    int bit_depth;
    int color_type;
    int interlacing;
    int comp_mode;
    int filter_method;

    png_read_info(png_ptr, info_ptr);
    bool error = png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlacing, &comp_mode, &filter_method);

    header.width = width;
    header.height = height;
    header.bdepth = static_cast<uint8_t>(bit_depth);
    header.colortype = static_cast<uint8_t>(color_type);
    header.interlacing = static_cast<uint8_t>(interlacing);
    header.compMode = static_cast<uint8_t>(comp_mode);
    header.filterMethod = static_cast<uint8_t>(filter_method);

    return error;
}

static bool loadRGBA(std::vector<float> &data, png_structp png_ptr, png_infop info_ptr, PNGheader header)
{
    if (header.bdepth == 16)
        png_set_strip_16(png_ptr);

    if (header.colortype == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png_ptr);

    if (header.colortype == PNG_COLOR_TYPE_GRAY && header.bdepth < 8)
        png_set_expand_gray_1_2_4_to_8(png_ptr);

    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS) != 0)
        png_set_tRNS_to_alpha(png_ptr);

    if (header.colortype == PNG_COLOR_TYPE_RGB ||
        header.colortype == PNG_COLOR_TYPE_GRAY ||
        header.colortype == PNG_COLOR_TYPE_PALETTE)
        png_set_filler(png_ptr, 0xFF, PNG_FILLER_AFTER);

    if (header.colortype == PNG_COLOR_TYPE_GRAY ||
        header.colortype == PNG_COLOR_TYPE_GRAY_ALPHA)
        png_set_gray_to_rgb(png_ptr);

    png_read_update_info(png_ptr, info_ptr);

    png_bytep *row_pointers = (png_bytep *)malloc(sizeof(png_bytep) * header.height);
    for (int y = 0; y < header.height; y++)
    {
        row_pointers[y] = (png_byte *)malloc(png_get_rowbytes(png_ptr, info_ptr));
        if (row_pointers[y] == nullptr)
        {
            return false;
        }
    }

    png_read_image(png_ptr, row_pointers);

    png_read_end(png_ptr, info_ptr);

    for (unsigned int y = 0; y < header.height; y++)
    {
        for (unsigned int x = 0; x < header.width; x++)
        {
            unsigned int i = x + y * header.width;
            png_bytep pixel = &row_pointers[y][x * 4];
            data[i * 4 + 0] = pixel[0] / 255.f;
            data[i * 4 + 1] = pixel[1] / 255.f;
            data[i * 4 + 2] = pixel[2] / 255.f;
            data[i * 4 + 3] = pixel[3] / 255.f;
        }
    }

    for (int y = 0; y < header.height; y++)
    {
        free(row_pointers[y]);
    }
    free(row_pointers);

    return true;
}

bool PNGFileFormat::load(std::filesystem::path path)
{
    FILE *file = fopen(path.string().c_str(), "rb");

    if (!file)
    {
#ifdef _DEBUG
        std::cerr << "PNGGileFormat::load : unable to open the file" << std::endl;
#endif
        return false;
    }

    png_byte signature[8];

    if (fread(signature, 1, 8, file) != 8)
    {
#ifdef _DEBUG
        std::cerr << "PNGFileFormat::load : unable to read the file" << std::endl;
#endif
        fclose(file);
        return false;
    }

    if (png_sig_cmp(signature, 0, 8))
    {
#ifdef _DEBUG
        std::cerr << "PNGFileFormat::load : invalid PNG file" << std::endl;
#endif
        fclose(file);
        return false;
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
    if (!png_ptr)
    {
#ifdef _DEBUG
        std::cerr << "PNGFileFormat::load : failed to create png read struct" << std::endl;
#endif
        fclose(file);
        return false;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
#ifdef _DEBUG
        std::cerr << "PNGFileFormat::load : failed to create png info struct" << std::endl;
#endif
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fclose(file);
        return false;
    }

    png_infop end_info = png_create_info_struct(png_ptr);
    if (!end_info)
    {
#ifdef _DEBUG
        std::cerr << "PNGFileFormat::load : failed to create png end info struct" << std::endl;
#endif
        png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
        fclose(file);
        return false;
    }

    png_set_sig_bytes(png_ptr, 8);

    png_init_io(png_ptr, file);

    PNGheader header;
    loadHeader(header, png_ptr, info_ptr);

    m_width = header.width;
    m_height = header.height;

    m_pixel_buffer = std::vector<float>(m_width * m_height * 4);
    loadRGBA(m_pixel_buffer, png_ptr, info_ptr, header);

    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    fclose(file);

    return true;
}

void savePass(unsigned int pass, const std::vector<float> &pixel_buffer, png_structp png_ptr, png_bytep row, unsigned int width, unsigned int height)
{
    for (unsigned int y = 0; y < height; y++)
    {
        for (unsigned int x = 0; x < width; x++)
        {
            unsigned int i = x + y * width;
            png_bytep pixel = &row[x * 4];
            pixel[0] = static_cast<png_byte>(pixel_buffer[i * 4 + 0] * 255);
            pixel[1] = static_cast<png_byte>(pixel_buffer[i * 4 + 1] * 255);
            pixel[2] = static_cast<png_byte>(pixel_buffer[i * 4 + 2] * 255);
            pixel[3] = static_cast<png_byte>(pixel_buffer[i * 4 + 3] * 255);
        }

        png_write_row(png_ptr, row);

        // float progress = static_cast<float>(y + 1) / m_height;
#ifdef PNG_WRITE_INTERLACING_SUPPORTED
        // progress *= static_cast<float>(pass + 1) / num_pass;
#endif
        // progress_callback(progress);
    }
}

bool PNGFileFormat::save(std::filesystem::path path) const
{
    FILE *file = fopen(path.string().c_str(), "wb");

    if (!file)
    {
#ifdef _DEBUG
        std::cerr << "Image::save : Failed to create the file" << std::endl;
#endif
        return false;
    }

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
    {
        return false;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (!info_ptr)
    {
#ifdef _DEBUG
        std::cout << "PNGFileFormat::save : failed to create png info struct" << std::endl;
#endif
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        return false;
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        return false;
    }

    png_init_io(png_ptr, file);

    png_set_IHDR(png_ptr, info_ptr, m_width, m_height, 8,
                 PNG_COLOR_TYPE_RGB_ALPHA,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);

    png_timep png_date = (png_timep)malloc(sizeof(png_time));
    time_t now = time(0);
    tm *date = gmtime(&now);
    png_date->year = date->tm_year + 1900;
    png_date->month = date->tm_mon + 1;
    png_date->day = date->tm_mday;
    png_date->hour = date->tm_hour;
    png_date->minute = date->tm_min;
    png_date->second = date->tm_sec;
    png_set_tIME(png_ptr, info_ptr, png_date);

    png_write_info(png_ptr, info_ptr);

    png_bytep row = (png_bytep)malloc(sizeof(png_byte) * m_width * 4);

    if (row == nullptr)
    {
#ifdef _DEBUG
        std::cerr << "Image::save : failed to allocate memory" << std::endl;
#endif
        return false;
    }

    int num_pass;
#ifdef PNG_WRITE_INTERLACING_SUPPORTED
    num_pass = png_set_interlace_handling(png_ptr);
#else
    num_pass = 1;
#endif

    for (unsigned int pass = 0; pass < num_pass; pass++)
    {
        savePass(pass, m_pixel_buffer, png_ptr, row, m_width, m_height);
    }

    png_write_end(png_ptr, info_ptr);

    free(row);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(file);

    return true;
}