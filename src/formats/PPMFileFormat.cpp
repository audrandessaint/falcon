#include "../FileFormat.hpp"

#include <fstream>

using namespace falcon;

struct PPMheader
{
    std::int32_t width;  /// width of the image
    std::int32_t height; /// height of the image
    std::int32_t maxVal; /// maximal value of r, g and b componants
};

static bool loadHeader(PPMheader &header, std::istream &file)
{
    char signature[2];

    file.read(signature, sizeof(signature));
    if (signature[0] != 'P' || signature[1] != '6')
        return false;

    file >> header.width;
    file >> header.height;
    file >> header.maxVal;

    file.get();

    if (header.width < 0 || header.height < 0 || header.maxVal < 0 || header.maxVal >= 65536 || file.fail())
    {
        return false;
    }

    return true;
}

#include <iostream>

static bool loadRGBData(std::vector<float> &data, std::istream &file, const PPMheader &header)
{
    uint8_t nbytes_per_channel = 1 + header.maxVal / 256;

    uint16_t r = 0;
    uint16_t g = 0;
    uint16_t b = 0;

    for (int i = 0; i < header.width * header.height; i++)
    {
        file.read(reinterpret_cast<char *>(&r), nbytes_per_channel);
        file.read(reinterpret_cast<char *>(&g), nbytes_per_channel);
        file.read(reinterpret_cast<char *>(&b), nbytes_per_channel);

        data[i * 4 + 0] = r / static_cast<float>(header.maxVal);
        data[i * 4 + 1] = g / static_cast<float>(header.maxVal);
        data[i * 4 + 2] = b / static_cast<float>(header.maxVal);
        data[i * 4 + 3] = 1.f;
    }

    return !file.fail();
}

static void saveHeader(const PPMheader &header, std::ostream &file)
{
    file << "P6"
         << "\n";
    file << header.width << " " << header.height << "\n";
    file << header.maxVal << "\n";
}

static void saveRGBData(const std::vector<float> &data, std::ofstream &file, const PPMheader &header)
{
    for (int i = 0; i < header.width * header.height; i++)
    {
        uint8_t r = static_cast<uint8_t>(data[i * 4 + 0] * 255);
        uint8_t g = static_cast<uint8_t>(data[i * 4 + 1] * 255);
        uint8_t b = static_cast<uint8_t>(data[i * 4 + 2] * 255);

        uint8_t pixel_data[] = {r, g, b};
        file.write(reinterpret_cast<char *>(pixel_data), 3);

        // progress_callback(static_cast<float>(i + 1) / (header.width * header.height));
    }
}

bool PPMFileFormat::load(std::filesystem::path path)
{
    std::ifstream file;

    file.open(path, std::ios::in | std::ios::binary);
    if (!file.is_open())
    {
#ifdef _DEBUG
        std::cerr << "Image::load : enable to open the file" << std::endl;
#endif
        return false;
    }

    PPMheader header;

    if (!loadHeader(header, file))
    {
#ifdef _DEBUG
        std::cerr << "Image::laod : invalid PPM file" << std::endl;
#endif
        return false;
    }

    m_pixel_buffer = std::vector<float>(header.width * header.height * 4);

    if (!loadRGBData(m_pixel_buffer, file, header))
    {
#ifdef _DEBUG
        std::cerr << "Image::laod : invalid PPM file" << std::endl;
#endif
        return false;
    }

    file.close();

    m_width = header.width;
    m_height = header.height;

    return true;
}

bool PPMFileFormat::save(std::filesystem::path path) const
{
    std::ofstream file;

    file.open(path, std::ios::out | std::ios::binary);
    if (!file.is_open())
    {
#ifdef _DEBUG
        std::cout << "Image::save : failed to create the file" << std::endl;
#endif
        return false;
    }

    PPMheader header;
    header.width = m_width;
    header.height = m_height;
    header.maxVal = 255;

    saveHeader(header, file);
    saveRGBData(m_pixel_buffer, file, header);

    file.close();
    return true;
}
