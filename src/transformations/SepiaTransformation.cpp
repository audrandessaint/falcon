#include "../Transformation.hpp"

using namespace falcon;

Color SepiaTransformation::operator()(const Image::Pixel &pixel)
{
    const Color color = pixel.color;
    float r = color.red() * 0.393f + color.green() * 0.769f + color.blue() * 0.189f;
    float g = color.red() * 0.349f + color.green() * 0.686f + color.blue() * 0.168f;
    float b = color.red() * 0.272f + color.green() * 0.534f + color.blue() * 0.131f;
    float a = color.alpha();

    return Color(r, g, b, a);
}