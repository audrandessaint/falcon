#include "../Transformation.hpp"

using namespace falcon;

BinaryTransformation::BinaryTransformation(float threshold) : m_threshold(threshold)
{
}

Color BinaryTransformation::operator()(const Image::Pixel &pixel)
{
    return pixel.color.gray() < m_threshold ? Color(0.f, 0.f, 0.f) : Color(1.f, 1.f, 1.f);
}