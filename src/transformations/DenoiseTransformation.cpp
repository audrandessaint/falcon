#include "../Transformation.hpp"

#include <OpenImageDenoise/oidn.hpp>

using namespace falcon;

DenoiseTransformation::DenoiseTransformation(const DenoiseInfo &denoise_info) : m_prefilter_albedo(denoise_info.prefilter_albedo),
                                                                                m_prefilter_normal(denoise_info.prefilter_normal)
{
    unsigned int albedo_size = denoise_info.albedo_pass.width() * denoise_info.albedo_pass.height();
    m_albedo_image.resize(albedo_size * 3);
    const Color *albedo_pixels = denoise_info.albedo_pass.pixels();
    for (unsigned int i = 0; i < albedo_size; i++)
    {
        m_albedo_image[i * 3 + 0] = albedo_pixels[i].red();
        m_albedo_image[i * 3 + 1] = albedo_pixels[i].green();
        m_albedo_image[i * 3 + 2] = albedo_pixels[i].blue();
    }

    unsigned int normal_size = denoise_info.normal_pass.width() * denoise_info.normal_pass.height();
    m_normal_image.resize(normal_size * 3);
    const Color *normal_pixels = denoise_info.normal_pass.pixels();
    for (unsigned int i = 0; i < normal_size; i++)
    {
        m_normal_image[i * 3 + 0] = normal_pixels[i].red();
        m_normal_image[i * 3 + 1] = normal_pixels[i].green();
        m_normal_image[i * 3 + 2] = normal_pixels[i].blue();
    }
}

void DenoiseTransformation::setReferenceImage(const Image &image)
{
    GlobalTransformation::setReferenceImage(image);

    oidn::DeviceRef device = oidn::newDevice();
    device.commit();

    unsigned int width = image.width();
    unsigned int height = image.height();

    std::vector<float> image_data(width * height * 3);

    for (unsigned int x = 0; x < width; x++)
    {
        for (unsigned int y = 0; y < height; y++)
        {
            image_data[(x + y * width) * 3 + 0] = image.getPixel(x, y).red();
            image_data[(x + y * width) * 3 + 1] = image.getPixel(x, y).green();
            image_data[(x + y * width) * 3 + 2] = image.getPixel(x, y).blue();
        }
    }

    oidn::FilterRef filter = device.newFilter("RT");
    filter.setImage("color", image_data.data(), oidn::Format::Float3, width, height);

    if (m_albedo_image.size() == width * height)
    {
        filter.setImage("albedo", m_albedo_image.data(), oidn::Format::Float3, width, height);
    }

    if (m_normal_image.size() == width * height)
    {
        filter.setImage("normal", m_normal_image.data(), oidn::Format::Float3, width, height);
    }

    filter.setImage("output", image_data.data(), oidn::Format::Float3, width, height);
    filter.set("hdr", true);
    filter.commit();

    if (m_prefilter_albedo)
    {
        oidn::FilterRef albedoFilter = device.newFilter("RT");
        albedoFilter.setImage("albedo", m_albedo_image.data(), oidn::Format::Float3, width, height);
        albedoFilter.setImage("output", m_albedo_image.data(), oidn::Format::Float3, width, height);
        albedoFilter.commit();
        albedoFilter.execute();
    }

    if (m_prefilter_normal)
    {
        oidn::FilterRef normalFilter = device.newFilter("RT");
        normalFilter.setImage("normal", m_normal_image.data(), oidn::Format::Float3, width, height);
        normalFilter.setImage("output", m_normal_image.data(), oidn::Format::Float3, width, height);
        normalFilter.commit();
        normalFilter.execute();
    }

    filter.execute();

    m_image = Image(width, height);

    const char *errorMessage;
    if (device.getError(errorMessage) != oidn::Error::None)
    {
#ifdef _DEBUG
        std::cerr << "DenoiseTransformation::setReferenceImage : " << errorMessage << std::endl;
#endif
        return;
    }

    for (unsigned int x = 0; x < width; x++)
    {
        for (unsigned int y = 0; y < height; y++)
        {
            float r = image_data[(x + y * width) * 3 + 0];
            float g = image_data[(x + y * width) * 3 + 1];
            float b = image_data[(x + y * width) * 3 + 2];
            m_image.setPixel({x, y, Color(r, g, b, 1.f)});
        }
    }
}

Color DenoiseTransformation::operator()(unsigned int x, unsigned int y)
{
    return m_image.getPixel(x, y);
}