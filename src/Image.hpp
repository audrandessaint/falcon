#pragma once

#include <string>
#include <cstring>
#include <memory>
#include <functional>
#include <vector>

#include "Color.hpp"
#include "FileFormat.hpp"

namespace falcon
{
    class LocalTransformation;
    class GlobalTransformation;
    template <unsigned int N>
    class TransformationSIMD;
    class LocalComputation;
    template <unsigned int N>
    class ComputationSIMD;

    class Image
    {
    public:
        struct DenoiseInfo;

        struct Pixel
        {
            unsigned int x;
            unsigned int y;
            Color color;
        };

        Image() : Image(0, 0) {}
        Image(unsigned int, unsigned int);
        Image(unsigned int, unsigned int, const Color &);
        Image(unsigned int, unsigned int, const uint8_t *);
        Image(unsigned int, unsigned int, const float *);
        Image(unsigned int, unsigned int, const Color *);
        Image(std::filesystem::path, std::shared_ptr<FileFormat>);
        Image(const Image &);
        ~Image();

        void operator=(const Image &);

        bool load(std::filesystem::path, std::shared_ptr<FileFormat>);
        bool save(std::filesystem::path, std::shared_ptr<FileFormat>) const;

        inline bool loaded() const { return m_opened; }
        explicit operator bool() const { return m_opened; }

        inline unsigned int width() const { return m_width; }
        inline unsigned int height() const { return m_height; }

        inline Color *pixels() { return m_pixels; }
        inline const Color *pixels() const { return m_pixels; }

        inline void setPixel(const Pixel &pixel) { m_pixels[pixel.x + pixel.y * m_width] = pixel.color; }
        inline const Color &getPixel(unsigned int x, unsigned int y) const { return m_pixels[x + y * m_width]; }

        Color getInterpolatedPixel(float, float) const;

        void clear();

        void fill(const Color &);
        void fill(const Color &, unsigned int, unsigned int, unsigned int, unsigned int);

        Color getAverageColor() const;

        void apply(std::shared_ptr<LocalTransformation>);
        void apply(std::shared_ptr<GlobalTransformation>);

        template <unsigned int N>
        void apply(std::shared_ptr<TransformationSIMD<N>>);

        void execute(std::shared_ptr<LocalComputation>);

        template <unsigned int N>
        void execute(std::shared_ptr<ComputationSIMD<N>>);

    public:
        friend bool operator==(const Image &, const Image &);
        friend bool operator!=(const Image &, const Image &);
        friend Image operator-(const Image &, const Image &);

    private:
        bool m_opened;
        unsigned int m_width;
        unsigned int m_height;
        Color *m_pixels;
    };

    struct Image::DenoiseInfo
    {
        Image albedo_map;
        Image normal_map;
        bool hdr = false;
    };

    template <unsigned int N>
    void Image::apply(std::shared_ptr<TransformationSIMD<N>> transformation)
    {
        for (unsigned int i = 0; i < (m_width * m_height - 1) / N; i++)
        {
            (*transformation)(&m_pixels[i * N]);
        }

        unsigned int n = ((m_width * m_height - 1) / N) * N;

        Color *buffer = new Color[N];
        std::memset(buffer, 0, N * sizeof(Color));
        std::memcpy(buffer, &m_pixels[n], (m_width * m_height - 1 - n) * sizeof(Color));

        (*transformation)(buffer);

        for (unsigned int i = n; i < m_width * m_height - 1; i++)
        {
            m_pixels[i] = buffer[i - n];
        }
    }
}