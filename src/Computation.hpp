#pragma once

#include "Image.hpp"

namespace falcon
{
    class LocalComputation
    {
    public:
        virtual void operator()(const Image::Pixel &) = 0;
    };

    class AverageColorComputation : public LocalComputation
    {
    public:
        AverageColorComputation();
        virtual ~AverageColorComputation();

        virtual void operator()(const Image::Pixel &);

        Color getAverageColor() const;

    private:
        unsigned int m_pixel_count;
        Color m_color_sum;
    };

    class HistogramComputation : public LocalComputation
    {
    public:
        HistogramComputation(unsigned int);
        virtual ~HistogramComputation();

        virtual void operator()(const Image::Pixel &);

        const std::vector<unsigned int> &getHistogram() const;

    private:
        std::vector<unsigned int> m_histogram;
    };
};