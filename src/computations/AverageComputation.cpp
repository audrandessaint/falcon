#include "../Computation.hpp"

using namespace falcon;

AverageColorComputation::AverageColorComputation() : m_pixel_count(0), m_color_sum(0)
{
}

void AverageColorComputation::operator()(const Image::Pixel &pixel)
{
    m_pixel_count++;
    m_color_sum += pixel.color;
}

Color AverageColorComputation::getAverageColor() const
{
    return m_color_sum / m_pixel_count;
}