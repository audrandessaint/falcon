#set(OIDN_INSTALL_DEPENDENCIES ON) # not necessay on current computer but that could be explained by the fact that the dependencies are already installed ?
add_subdirectory(oidn)
include_directories(${PROJECT_NAME} PRIVATE oidn/include)
target_link_libraries(${PROJECT_NAME} PRIVATE OpenImageDenoise)