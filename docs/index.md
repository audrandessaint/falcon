# Image Processing Library Documentation


## Getting started

### Build with CMake

#### Prerequisites

- [Git LFS](https://git-lfs.com/)
- [CMake](http://www.cmake.org/)
- [ISPC](https://ispc.github.io/downloads.html)
- [Ninja](https://ninja-build.org/)
- ([clang]())


#### Clone repository

```bash
git clone --recursive https://gitlab.com/audrandessaint/falcon.git
```


#### Install VCPKG

#### Linux

```bash
./vendors/vcpkg/bootstrap-vcpkg.sh
```

#### Windows

Use the x86_x64 vs 2022 terminal (everything should be configured correctly on it)

```powershell
.\vendors\vcpkg\bootstrap-vcpkg.bat
```

Then, to build the library with CMake enter the following commands from the root folder of the project:

```bash
cmake -G Ninja -B build -S .
cmake --build build
```

### Colors

[TODO]

### Load and Save Images

```c++
falcon::Image image;
bool loaded = image.load("path/to/my/image.png", std::make_shared<falcon::PNGFileFormat>());

if (!loaded) { /* handle error */ }
```

```c++
bool saved = image.save("path/to/my/image.png", std::make_shared<falcon::PNGFileFormat>());

if (!saved) { /* handle error */ }
```

#### Asynchronous loading and progress report

If you want to load your images asynchronously, for example if you load many heavy files and do not want to block your UI, it is possible to get progress information on your load and save operations.

```c++
auto loader = std::make_shared<falcon::PNGFileFormat>();

std::async(&falcon::Image::load, &image, "path/to/my/image.png", loader);

std::cout << loader->progress() << std::endl;
```

### Apply Transformations

Applying transformations to images follows the same pattern as loading and saving them.

```c++
auto transformation = std::make_shared<falcon::GammaCorrection>();
image.apply(transformation);
```

#### Built-in Transformations

- Binary filter
- Histogram egalization
- Horizontal and vertical flip
- Denoising for ray traced images
- Edged Detection
- Gamma correction
- Sepia filter
- Convolution filters

## Extension API

The library is design to be extended and customized to specific needs. Its extensions API allows to register new image file formats and new image transformations.

### Add support for an image file format

[TODO]

### Add support for a custom image transformation

Local transformations are performed in place:
```c++
class NewTransformation : public falcon::LocalTransformation
{
public:
    virtual Color operator()(const falcon::Image::Pixel& pixel)
    {
        // implementation
    }
}
```

Global transformations are needed when the transformation cannot be performed in place and a copy is required. This copy is accessible as the protected member `m_image`:

```c++
class NewTransformation : public falcon::GlobalTransformation
{
public:
    virtual Color operator()(unsigned int x, unsigned int y);
}
```

If you want to use SIMD operations you should use the `SIMD16Transformation` base class. Transformations inherinting from this class will be fed 16 pixel colors at a time, even though the image does not have a number of pixels divisible by 16.
```c++
class NewTransformation : public falcon::SIMD16Transformation
{
public:
    virtual void operator()(Color*);
}