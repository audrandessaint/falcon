#include <iostream>
#include <memory>

#include "../include/falcon.hpp"

int main(int argc, char *argv[])
{
    falcon::Image image("../samples/sample_640x426-result.png", std::make_unique<falcon::PNGFileFormat>());
    falcon::Image albedo("../samples/albedo.png", std::make_unique<falcon::PNGFileFormat>());
    falcon::Image normals("../samples/normals.png", std::make_unique<falcon::PNGFileFormat>());

    if (!image.loaded())
    {
        std::cerr << "Failed to load the image from disk" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Image loaded successfully" << std::endl;
    std::cout << "width: " << image.width() << std::endl;
    std::cout << "height: " << image.height() << std::endl;

    falcon::DenoiseTransformation::DenoiseInfo denoise_info;
    denoise_info.albedo_pass = albedo;
    denoise_info.normal_pass = normals;
    denoise_info.prefilter_albedo = false;
    denoise_info.prefilter_normal = false;

    falcon::ConvolutionTransformation::Filter filter;
    filter.kernel_size = 1;
    filter.coefficients = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    auto computation = std::make_shared<falcon::HistogramComputation>(300);
    // image.execute(computation);
    // image.apply(std::make_shared<falcon::ContrastTransformation>(computation->getHistogram()));
    // image.apply(std::make_shared<falcon::ConvolutionTransformation>(filter));
    // image.apply<16>(std::make_shared<falcon::NegativeTransformationSIMD<16>>());

    if (!image.save("../samples/denoise-result.png", std::make_unique<falcon::PNGFileFormat>()))
    {
        std::cerr << "Failed to save the image" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Image saved successfully" << std::endl;

    return EXIT_SUCCESS;
}